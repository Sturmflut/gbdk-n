#include <gb/gb.h>

#include "tiles.h"

void main()
{
    UBYTE jet_x = 70;
    UBYTE jet_y = 140;

    UBYTE bullet_x = 161;
    UBYTE bullet_y = 0;

    UBYTE i;

    // Init palette (Black, dark grey, light grey, transparent)
    OBP0_REG = 0xE4;

    // Init jet sprite
    SPRITES_8x8;
    set_sprite_data(0, 5, tiles);
    set_sprite_tile(SPRITE_ID_JET, SPRITE_ID_JET);
    set_sprite_tile(SPRITE_ID_JET + 1, SPRITE_ID_JET + 1);
    set_sprite_tile(SPRITE_ID_JET + 2, SPRITE_ID_JET + 2);
    set_sprite_tile(SPRITE_ID_JET + 3, SPRITE_ID_JET + 3);
    //set_sprite_prop(0, 0x00);
    //set_sprite_prop(1, 0x00);
    //set_sprite_prop(2, 0x00);
    //set_sprite_prop(3, 0x00);

    // Init bullet sprite
    set_sprite_tile(SPRITE_ID_BULLET, SPRITE_ID_BULLET);
    set_sprite_prop(SPRITE_ID_BULLET, 0x00);
    move_sprite(SPRITE_ID_BULLET, bullet_x, bullet_y);

    SHOW_SPRITES;

    // Sound effect
    NR52_REG = 0x80;
    NR51_REG = 0x11;
    NR50_REG = 0x77;

    NR10_REG = 0x1E;
    NR11_REG = 0x10;
    NR12_REG = 0xF3;
    NR13_REG = 0x00;

    // Animate
    while(1)
    {
        if(bullet_x != 161)
	{
            bullet_y -= 5;

	    if(bullet_y < 0 || bullet_y > 150)
                bullet_x = 161;
	}

	// Handle joypad
	i = joypad();

	// Move jet left
	if(i & J_LEFT)
	    jet_x--;

	// Move jet right
	if(i & J_RIGHT)
	    jet_x++;

	// Fire bullet, if not yet fired
	if((i & J_A) && (bullet_x == 161))
	{
	    bullet_x = jet_x + 4;
	    bullet_y = jet_y;
	    NR14_REG = 0x87;
	}

        move_sprite(0, jet_x, jet_y);
        move_sprite(1, jet_x, jet_y + 8);
        move_sprite(2, jet_x + 8, jet_y);
        move_sprite(3, jet_x + 8, jet_y + 8);

	move_sprite(4, bullet_x, bullet_y);

        wait_vbl_done();
        wait_vbl_done();
    }
}
