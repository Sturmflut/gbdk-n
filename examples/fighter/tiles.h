#ifndef TILES_H_
#define TILES_H_

#define SPRITE_ID_JET 0
#define SPRITE_ID_BULLET 4

unsigned char tiles[] =
{
    // Jet
    0x00,0x00,0x01,0x01,0x03,0x03,0x07,0x07,
    0x07,0x03,0x03,0x03,0x0F,0x0F,0x1F,0x1F,
    0x3F,0x3F,0x7F,0x7F,0xFF,0x83,0x03,0x03,
    0x03,0x03,0x07,0x07,0x0F,0x0B,0x03,0x01,
    0x00,0x00,0x00,0x00,0x80,0x80,0xC0,0xC0,
    0xC0,0x80,0x80,0x80,0xE0,0xE0,0xF0,0xF0,
    0xF8,0xF8,0xFC,0xFC,0xFE,0x82,0x80,0x80,
    0x80,0x80,0xC0,0xC0,0xE0,0xA0,0x80,0x00,

    // Bullet
    0x00,0x00,0x00,0x00,0x00,0x00,0x10,0x10,
    0x10,0x10,0x10,0x10,0x00,0x00,0x00,0x00
};

#endif
