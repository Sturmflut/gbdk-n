#include <gb/gb.h>

#define NUM_BALLS 5

unsigned char ball_tile[] =
{
  0x00,0x18,0x00,0x24,0x00,0x42,0x00,0x81,
  0x00,0x81,0x00,0x42,0x00,0x24,0x00,0x18
};

void main()
{
    UBYTE x[NUM_BALLS];
    UBYTE y[NUM_BALLS];
    BYTE xdelta[NUM_BALLS];
    BYTE ydelta[NUM_BALLS];
    UBYTE i;

    // Init sprite
    SPRITES_8x8;
    set_sprite_data(0, 1, ball_tile);

    // Init balls
    for(i = 0; i < NUM_BALLS; i++)
    {
        set_sprite_tile(i,0);
        set_sprite_prop(i, 0x10);

        x[i] = (i + 1) * (150 / NUM_BALLS);
        y[i] = (i + 1) * (140 / NUM_BALLS);
        xdelta[i] = i % 2 ? -1 : 1;
        ydelta[i] = i % 2 ? 1 : -1;
    }

    // Show sprites
    SHOW_SPRITES;

    // Sound effect
    NR52_REG = 0x80;
    NR51_REG = 0x11;
    NR50_REG = 0x77;

    NR10_REG = 0x1E;
    NR11_REG = 0x10;
    NR12_REG = 0xF3;
    NR13_REG = 0x00;

    // Animate
    while(1)
    {
        for(i = 0; i < NUM_BALLS; i++)
        {
            move_sprite(i, x[i], y[i]);

	    x[i] += xdelta[i];
	    y[i] += ydelta[i];

	    // Bump at borders and play sound effect
	    if(x[i] == 8 && xdelta[i] == -1)
            {
                xdelta[i] = 1;
                NR14_REG = 0x87;
            }

            if(x[i] == 160 && xdelta[i] == 1)
            {
                xdelta[i] = -1;
                NR14_REG = 0x87;
            }

            if(y[i] == 16 && ydelta[i] == -1)
            {
	        ydelta[i] = 1;
                NR14_REG = 0x87;
            }

            if(y[i] == 150 && ydelta[i] == 1)
            {
                ydelta[i] = -1;
                NR14_REG = 0x87;
            }
	}

	// Wait for blank
        wait_vbl_done();
        wait_vbl_done();
    }
}
