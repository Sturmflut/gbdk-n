echo off
set BIN=..\..\bin
set OBJ=obj

if "%1"=="clean" (
	if exist %OBJ% rd /s/q %OBJ%
	if exist balls.gb del balls.gb
	goto end
)

if not exist %OBJ% mkdir %OBJ%
call %BIN%\gbdk-n-compile.bat balls.c -o %OBJ%\balls.rel
call %BIN%\gbdk-n-link.bat %OBJ%\balls.rel -o %OBJ%\balls.ihx
call %BIN%\gbdk-n-make-rom.bat %OBJ%\balls.ihx balls.gb

:end
